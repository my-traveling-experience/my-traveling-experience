import 'package:meta/meta.dart';
import 'package:mytravelingexperience/models/user.dart' as model;

abstract class AppEvent {
  const AppEvent();
}

class AppStarted extends AppEvent {}

class AppLoggedIn extends AppEvent {
  final model.User user;

  const AppLoggedIn({@required this.user});
}

class AppLoggedOut extends AppEvent {}
