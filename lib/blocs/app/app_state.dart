import 'package:meta/meta.dart';
import 'package:mytravelingexperience/models/user.dart' as model;

abstract class AppState {
  const AppState();
}

class AppUninitialized extends AppState {}

class AppInitialized extends AppState {}

class AppAuthenticated extends AppState {
  final model.User user;

  const AppAuthenticated({@required this.user});
}

class AppUnauthenticated extends AppState {}

class AppLoading extends AppState {}
