import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/repositories/firebase_repository.dart';

import 'app_event.dart';
import 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final FirebaseRepository firebaseRepository;

  AppBloc({@required this.firebaseRepository}) : super(AppUninitialized());

  @override
  Future close() {
    return super.close();
  }

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is AppStarted) {
      // Check has already been logged
      yield AppLoading();
      yield AppUnauthenticated();
    } else if (event is AppLoggedIn) {
      yield AppLoading();
      yield AppAuthenticated(user: event.user);
    } else if (event is AppLoggedOut) {
      yield AppLoading();
      await firebaseRepository.logOut();
      yield AppUnauthenticated();
    }
  }
}
