import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FirebaseAuthEvent extends Equatable {
  const FirebaseAuthEvent();

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

class FirebaseAuthLoginButtonPressed extends FirebaseAuthEvent {
  final String email;
  final String password;

  const FirebaseAuthLoginButtonPressed({
    @required this.email,
    @required this.password,
  });
}

class FirebaseAuthRegisterButtonPressed extends FirebaseAuthEvent {
  final String email;
  final String username;
  final String password;

  const FirebaseAuthRegisterButtonPressed({
    @required this.email,
    @required this.username,
    @required this.password,
  });
}

class FirebaseAuthBackButtonPressed extends FirebaseAuthEvent {}

class FirebaseAuthLogout extends FirebaseAuthEvent {}
