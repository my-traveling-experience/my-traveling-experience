import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/blocs/app/app_bloc.dart';
import 'package:mytravelingexperience/blocs/app/app_event.dart';
import 'package:mytravelingexperience/models/user.dart' as model;

import 'auth_event.dart';
import 'auth_state.dart';

class FirebaseAuthBloc extends Bloc<FirebaseAuthEvent, FirebaseAuthState> {
  final AppBloc appBloc;

  FirebaseAuthBloc({@required this.appBloc})
      : assert(appBloc != null),
        super(FirebaseAuthInitial());

  @override
  Future close() {
    return super.close();
  }

  @override
  Stream<FirebaseAuthState> mapEventToState(FirebaseAuthEvent event) async* {
    if (event is FirebaseAuthLoginButtonPressed) {
      yield* _mapLoginToState(event);
    } else if (event is FirebaseAuthRegisterButtonPressed) {
      yield* _mapRegisterToState(event);
    } else if (event is FirebaseAuthBackButtonPressed) {
      yield FirebaseAuthInitial();
    }
  }

  // Handle Login logic
  Stream<FirebaseAuthState> _mapLoginToState(
      FirebaseAuthLoginButtonPressed event) async* {
    yield FirebaseAuthLoading();

    try {
      final res = await appBloc.firebaseRepository.logIn(
        email: event.email,
        password: event.password,
      );
      //TODO : Handle res == model.User.empty

      yield* _handleResponse(res);
    } catch (error) {
      yield* _handleError(error);
      rethrow;
    }
  }

  Stream<FirebaseAuthState> _mapRegisterToState(
    FirebaseAuthRegisterButtonPressed event,
  ) async* {
    yield FirebaseAuthLoading();

    try {
      final res = await appBloc.firebaseRepository.signUp(
        email: event.email,
        username: event.username,
        password: event.password,
      );
      //TODO : Handle res == model.User.empty

      yield* _handleResponse(res);
    } catch (error) {
      yield* _handleError(error);
      rethrow;
    }
  }

  Stream<FirebaseAuthState> _handleError(
    Exception error,
  ) async* {
    yield FirebaseAuthFailure(error.toString());
  }

  Stream<FirebaseAuthState> _handleResponse(
    model.User res,
  ) async* {
    appBloc.add(AppLoggedIn(user: res));
  }
}
