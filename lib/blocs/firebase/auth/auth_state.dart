abstract class FirebaseAuthState {
  const FirebaseAuthState();
}

class FirebaseAuthInitial extends FirebaseAuthState {}

class FirebaseAuthLoading extends FirebaseAuthState {}

class FirebaseAuthForgot extends FirebaseAuthState {
  /*final String msg;

  const LoginForgot(this.msg);

  @override
  String toString() => "$msg";*/
}

class FirebaseAuthPasswordChanged extends FirebaseAuthState {}

class FirebaseAuthFailure extends FirebaseAuthState {
  final String error;

  const FirebaseAuthFailure(this.error);

  @override
  String toString() => "$error";
}
