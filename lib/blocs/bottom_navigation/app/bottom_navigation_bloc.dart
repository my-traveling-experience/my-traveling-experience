import 'package:flutter_bloc/flutter_bloc.dart';

import 'bottom_navigation_event.dart';
import 'bottom_navigation_state.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  int currentIndex = 0;

  BottomNavigationBloc() : super(BottomNavigationLoadingScreen());

  @override
  Future close() {
    return super.close();
  }

  @override
  Stream<BottomNavigationState> mapEventToState(
      BottomNavigationEvent event) async* {
    if (event is BottomNavigationInitialized) {
      this.add(BottomNavigationTapped(index: currentIndex));
    } else if (event is BottomNavigationTapped) {
      currentIndex = event.index;

      yield BottomNavigationCurrentIndex(currentIndex: currentIndex);
      yield BottomNavigationLoadingScreen();

      switch (currentIndex) {
        case 0:
          yield BottomNavigationLoadFirstScreen();
          break;
        case 1:
          yield BottomNavigationLoadSecondScreen();
          break;
        case 2:
          yield BottomNavigationLoadThirdScreen();
          break;
        case 3:
          yield BottomNavigationLoadFourthScreen();
          break;
        case 4:
          yield BottomNavigationLoadFifthScreen();
          break;
        //default:
        //  yield BottomNavigationLoadingScreen();
        //  break;
      }
    }
  }
}
