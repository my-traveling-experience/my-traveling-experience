import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class BottomNavigationEvent extends Equatable {
  const BottomNavigationEvent();

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [];
}

class BottomNavigationInitialized extends BottomNavigationEvent {}

class BottomNavigationTapped extends BottomNavigationEvent {
  final int index;

  BottomNavigationTapped({@required this.index});
}
