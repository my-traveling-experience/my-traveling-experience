import 'package:meta/meta.dart';

abstract class BottomNavigationState {
  const BottomNavigationState();
}

class BottomNavigationCurrentIndex extends BottomNavigationState {
  final int currentIndex;

  BottomNavigationCurrentIndex({@required this.currentIndex});
}

class BottomNavigationLoadingScreen extends BottomNavigationState {}

class BottomNavigationLoadFirstScreen extends BottomNavigationState {}

class BottomNavigationLoadSecondScreen extends BottomNavigationState {}

class BottomNavigationLoadThirdScreen extends BottomNavigationState {}

class BottomNavigationLoadFourthScreen extends BottomNavigationState {}

class BottomNavigationLoadFifthScreen extends BottomNavigationState {}
