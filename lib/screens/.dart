import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mytravelingexperience/widgets/mte_container.dart';

class SavedScreen extends StatefulWidget {
  SavedScreen({Key key}) : super(key: key);

  @override
  _SavedScreenState createState() => _SavedScreenState();
}

class _SavedScreenState extends State<SavedScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MTEContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Saved screen"),
          ],
        ),
      ),
    );
  }
}
