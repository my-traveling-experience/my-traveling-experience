import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/blocs/bottom_navigation/app/bottom_navigation_bloc.dart';
import 'package:mytravelingexperience/blocs/bottom_navigation/app/bottom_navigation_state.dart';
import 'package:mytravelingexperience/models/user.dart' as model;
import 'package:mytravelingexperience/screens/explore_screen.dart';
import 'package:mytravelingexperience/screens/profile_screen.dart';
import 'package:mytravelingexperience/screens/saved_screen.dart';
import 'package:mytravelingexperience/screens/trips_screen.dart';
import 'package:mytravelingexperience/widgets/mte_bottom_navbar.dart';

class HomeScreen extends StatefulWidget {
  final model.User user;
  final bool loading;

  HomeScreen({Key key, this.user, this.loading}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  BottomNavigationBloc _bottomNavigationBloc;

  @override
  void initState() {
    _bottomNavigationBloc = BlocProvider.of<BottomNavigationBloc>(context);
    super.initState();
  }

  Widget _buildBodyScreen(BuildContext context) {
    return BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
      cubit: _bottomNavigationBloc,
      builder: (BuildContext context, BottomNavigationState state) {
        if (state is BottomNavigationLoadingScreen) {
          return Center(child: CircularProgressIndicator());
        } else if (state is BottomNavigationLoadFirstScreen) {
          return ExploreScreen();
        } else if (state is BottomNavigationLoadSecondScreen) {
          return SavedScreen();
        } else if (state is BottomNavigationLoadThirdScreen) {
          return TripsScreen();
        } else if (state is BottomNavigationLoadFourthScreen) {
          return ProfileScreen();
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: Stack(
        children: [
          _buildBodyScreen(context),
          Align(
            alignment: Alignment.bottomCenter,
            child: MTEBottomNavigationBar(),
          ),
        ],
      ),
    );
  }
}
