import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mytravelingexperience/widgets/mte_container_scrollable.dart';

class TripsScreen extends StatefulWidget {
  TripsScreen({Key key}) : super(key: key);

  @override
  _TripsScreenState createState() => _TripsScreenState();
}

class _TripsScreenState extends State<TripsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MTEContainerScrollable(
        scrollbarEnabled: true,
        padding: EdgeInsets.only(
          left: MediaQuery.of(context).padding.left + 24,
          right: MediaQuery.of(context).padding.top + 24,
          top: MediaQuery.of(context).padding.top + 12,
          bottom: MediaQuery.of(context).padding.bottom,
        ),
        children: <Widget>[
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
          Text("Trips screen"),
        ],
      ),
    );
  }
}
