import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mytravelingexperience/widgets/mte_button.dart';
import 'package:mytravelingexperience/widgets/mte_container.dart';

class SavedScreen extends StatefulWidget {
  SavedScreen({Key key}) : super(key: key);

  @override
  _SavedScreenState createState() => _SavedScreenState();
}

class _SavedScreenState extends State<SavedScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MTEContainer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Saved screen"),
            MTEElevatedButton(
              onPressed: () {},
              prefixIcon: Icon(Icons.access_alarm),
              //loading: true,
              //disabled: true,
              label: Text("toto"),
            ),
            MTEOutlinedButton(
              onPressed: () {},
              prefixIcon: Icon(Icons.access_alarm),
              suffixIcon: Icon(Icons.access_alarm),
              loading: true,
              disabled: true,
              label: Text("toto"),
            ),
          ],
        ),
      ),
    );
  }
}
