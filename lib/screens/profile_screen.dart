import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mytravelingexperience/widgets/mte_container.dart';
import 'package:mytravelingexperience/widgets/mte_maps.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MTEContainer(
      child: MTEMaps(),
    );
  }
}
