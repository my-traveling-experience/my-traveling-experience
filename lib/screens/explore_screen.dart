import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:mytravelingexperience/widgets/mte_container.dart';

enum FlightType { OneWayTrip, RoundTrip }

class ExploreScreen extends StatefulWidget {
  ExploreScreen({Key key}) : super(key: key);

  @override
  _ExploreScreenState createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {
  @override
  void initState() {
    super.initState();
  }

  FlightType flightType = FlightType.RoundTrip;
  final _formKey = GlobalKey<FormState>();

  Widget _buildForm(BuildContext context) {
    DateTime defaultStart = DateTime.now();
    DateTime defaultEnd = DateTime(
        DateTime.now().year, DateTime.now().month, DateTime.now().day + 13);
    TextEditingController _fromController = TextEditingController();
    TextEditingController _toController = TextEditingController();
    TextEditingController _dateStartController = TextEditingController(
        text: DateFormat().add_yMEd().format(defaultStart));
    TextEditingController _dateEndController =
        TextEditingController(text: DateFormat().add_yMEd().format(defaultEnd));

    _selectDate(BuildContext context) async {
      final DateTimeRange picked = await showDateRangePicker(
        context: context,
        firstDate: defaultStart,
        lastDate: DateTime(2050),
        initialDateRange: DateTimeRange(
          end: defaultEnd,
          start: defaultStart,
        ),
      );

      defaultEnd = picked.end;
      defaultStart = picked.start;

      _dateStartController.text = DateFormat().add_yMEd().format(picked.start);
      _dateEndController.text = DateFormat().add_yMEd().format(picked.end);
    }

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          DropdownButtonFormField<FlightType>(
            value: flightType,
            onChanged: (value) {
              setState(() {
                flightType = value;
              });
            },
            decoration: InputDecoration(
              isDense: true,
              prefixIcon: flightType == FlightType.OneWayTrip
                  ? Icon(Icons.trending_flat)
                  : Icon(Icons.sync_alt),
              border: OutlineInputBorder(),
            ),
            items: <FlightType>[FlightType.OneWayTrip, FlightType.RoundTrip]
                .map<DropdownMenuItem<FlightType>>((FlightType value) {
              Text flightTypeText;

              if (value == FlightType.OneWayTrip) {
                flightTypeText = Text('One way trip');
              } else {
                flightTypeText = Text('Rounded trip');
              }
              return DropdownMenuItem<FlightType>(
                  value: value, child: flightTypeText);
            }).toList(),
          ),
          SizedBox(height: 16),
          TextFormField(
            controller: _fromController,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.place),
              labelText: "Where from ?",
              hintText: "Paris",
              border: OutlineInputBorder(),
            ),
            //onFieldSubmitted: (e) => _onPressed(),
          ),
          SizedBox(height: 16),
          TextFormField(
            controller: _toController,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.place),
              labelText: "Where to ?",
              hintText: "Barcelona",
              border: OutlineInputBorder(),
            ),
            //onFieldSubmitted: (e) => _onPressed(),
          ),
          SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                child: TextFormField(
                  controller: _dateStartController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.date_range),
                    hintText: _dateStartController.text,
                    border: OutlineInputBorder(),
                  ),
                  onTap: () => _selectDate(context),
                  readOnly: true,
                  //onFieldSubmitted: (e) => _onPressed(),
                ),
              ),
              Visibility(
                visible: flightType == FlightType.RoundTrip,
                child: Expanded(
                  child: TextFormField(
                    controller: _dateEndController,
                    decoration: InputDecoration(
                      hintText: _dateEndController.text,
                      border: OutlineInputBorder(),
                    ),
                    onTap: () => _selectDate(context),
                    readOnly: true,
                    //onFieldSubmitted: (e) => _onPressed(),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MTEContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildForm(context),
          ],
        ),
      ),
    );
  }
}
