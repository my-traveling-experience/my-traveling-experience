import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:mytravelingexperience/blocs/app/app_bloc.dart';
import 'package:mytravelingexperience/blocs/app/app_event.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreen createState() => _SplashScreen();
}

class _SplashScreen extends State<SplashScreen> with TickerProviderStateMixin {
  AnimationController _controller;
  AppBloc _appBloc;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    _appBloc = BlocProvider.of<AppBloc>(context);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            "assets/lottiePlane.json",
            controller: _controller,
            onLoaded: (composition) {
              _controller.addStatusListener((status) {
                if (status == AnimationStatus.completed) {
                  _appBloc.add(AppStarted());
                }
              });
              _controller
                ..duration = composition.duration
                ..forward();
            },
          ),
        ],
      ),
    );
  }
}
