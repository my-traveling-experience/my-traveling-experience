import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/blocs/app/app_bloc.dart';
import 'package:mytravelingexperience/blocs/firebase/auth/auth_bloc.dart';
import 'package:mytravelingexperience/blocs/firebase/auth/auth_event.dart';
import 'package:mytravelingexperience/blocs/firebase/auth/auth_state.dart';
import 'package:mytravelingexperience/generated/l10n.dart';
import 'package:mytravelingexperience/widgets/mte_button.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

enum LoginMode { Login, Register }

class _LoginScreenState extends State<LoginScreen> {
  FirebaseAuthBloc _firebaseAuthBloc;

  LoginMode _loginMode = LoginMode.Login;

  var _formKey = GlobalKey<FormState>();

  bool _passwordVisible = false;

  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    _firebaseAuthBloc =
        FirebaseAuthBloc(appBloc: BlocProvider.of<AppBloc>(context));
    super.initState();
  }

  _onPageChange(mode) {
    if (mode == LoginMode.Login) {
      _firebaseAuthBloc.add(FirebaseAuthBackButtonPressed());
    }
    setState(() {
      _formKey = GlobalKey<FormState>();
      _loginMode = mode;
    });
  }

  _onPressed() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    if (_loginMode == LoginMode.Login) {
      _firebaseAuthBloc.add(FirebaseAuthLoginButtonPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ));
    } else if (_loginMode == LoginMode.Register) {
      _firebaseAuthBloc.add(FirebaseAuthRegisterButtonPressed(
        email: _emailController.text,
        username: _usernameController.text,
        password: _passwordController.text,
      ));
    }
  }

  List<Widget> _buildLoginForm() {
    return [
      TextFormField(
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: S.of(context).loginEmailAddress,
          hintText: "e.g. john.doe@gmail.com",
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginEmailAddressEmpty;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
      SizedBox(height: 16),
      TextFormField(
        controller: _passwordController,
        textInputAction: TextInputAction.go,
        obscureText: !_passwordVisible,
        decoration: InputDecoration(
          labelText: S.of(context).loginPassword,
          border: OutlineInputBorder(),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisible ? Icons.visibility : Icons.visibility_off,
              size: 20.0,
            ),
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
          ),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginPasswordEmpty;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
      SizedBox(height: 16),
    ];
  }

  List<Widget> _buildRegisterForm() {
    List<Widget> registerForm = [
      TextFormField(
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: S.of(context).loginEmailAddress,
          hintText: "e.g. john.doe@gmail.com",
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginEmailAddressEmpty;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
      SizedBox(height: 16),
      TextFormField(
        controller: _usernameController,
        decoration: InputDecoration(
          labelText: S.of(context).loginUsername,
          hintText: "e.g. john.doe44",
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginUsernameEmpty;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
      SizedBox(height: 16),
      TextFormField(
        controller: _passwordController,
        obscureText: !_passwordVisible,
        decoration: InputDecoration(
          labelText: S.of(context).loginPassword,
          border: OutlineInputBorder(),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisible ? Icons.visibility : Icons.visibility_off,
              size: 20.0,
            ),
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
          ),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginPasswordEmpty;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
      SizedBox(height: 16),
      TextFormField(
        obscureText: !_passwordVisible,
        textInputAction: TextInputAction.go,
        decoration: InputDecoration(
          labelText: S.of(context).loginConfirmPassword,
          border: OutlineInputBorder(),
          suffixIcon: IconButton(
            icon: Icon(
              _passwordVisible ? Icons.visibility : Icons.visibility_off,
              size: 20.0,
            ),
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
          ),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return S.of(context).loginConfirmPasswordEmpty;
          }
          if (value != _passwordController.text) {
            return S.of(context).loginConfirmPasswordIdentical;
          }
          return null;
        },
        onFieldSubmitted: (e) => _onPressed(),
      ),
    ];
    return registerForm;
  }

  List<Widget> _buildForm(state) {
    switch (_loginMode) {
      case LoginMode.Register:
        return _buildRegisterForm();
      default:
        return _buildLoginForm();
    }
  }

  List<Widget> _buildFooter(context, state) {
    String message;
    String buttonText;
    if (_loginMode == LoginMode.Register) {
      message = S.of(context).loginHaveAccount;
      buttonText = S.of(context).loginRegisterEmail;
    } else {
      message = S.of(context).loginNoAccount;
      buttonText = S.of(context).loginSignInEmail;
    }
    return [
      SizedBox(height: 16),
      SizedBox(
        width: double.infinity,
        child: MTEElevatedButton(
          label: Text(buttonText),
          onPressed: _onPressed,
          suffixIcon: Icon(Icons.arrow_right_alt),
        ),
      ),
      SizedBox(height: 16.0),
      Center(
        child: Opacity(
          opacity: 0.75,
          child: FlatButton(
            child: Text(message),
            onPressed: () => _onPageChange(_loginMode == LoginMode.Login
                ? LoginMode.Register
                : LoginMode.Login),
          ),
        ),
      ),
    ];
  }

  Widget _buildScreen(context, state) {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://images.unsplash.com/photo-1586348943529-beaae6c28db9?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Nnx8fGVufDB8fHw%3D&auto=format&fit=crop&w=600&q=60'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(26.0),
              child: Padding(
                padding: EdgeInsets.all(26.0),
                child: Column(
                  children: [
                    ..._buildForm(state),
                    ..._buildFooter(context, state),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _firebaseAuthBloc,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: BlocListener<FirebaseAuthBloc, FirebaseAuthState>(
              listener: (context, state) {
                if (state is FirebaseAuthInitial) {
                  _loginMode = LoginMode.Login;
                } else if (state is FirebaseAuthFailure) {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("$state"),
                    backgroundColor:
                        state is FirebaseAuthFailure ? Colors.red : null,
                  ));
                }
              },
              child: BlocBuilder<FirebaseAuthBloc, FirebaseAuthState>(
                  builder: (_, state) {
                return _buildScreen(context, state);
              }),
            ),
          ),
        ),
      ),
    );
  }
}
