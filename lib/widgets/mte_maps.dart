import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

class MTEMaps extends StatefulWidget {
  MTEMaps({Key key}) : super(key: key);

  @override
  _MTEMapsState createState() => _MTEMapsState();
}

class _MTEMapsState extends State<MTEMaps> {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _googleMapController;
  PermissionStatus _status;
  Position _currentPosition;

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  @override
  void initState() {
    super.initState();
    Permission.location.status.then(_updateStatus);
    if (!_status.isGranted) {
      _askPermission();
    } else {
      _locatePosition();
    }
  }

  void _updateStatus(PermissionStatus status) {
    if (status != _status) {
      setState(() {
        _status = status;
      });
    }
  }

  void _askPermission() {
    Permission.location.request().then(_onStatusRequest);
  }

  void _onStatusRequest(PermissionStatus status) {
    if (!status.isGranted) {
      //_askPermission();
      //openAppSettings();
    } else {
      _updateStatus(status);
      _locatePosition();
    }
  }

  void _locatePosition() async {
    if (_status.isGranted) {
      _currentPosition = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      LatLng latLngPosition = LatLng(
        _currentPosition.latitude,
        _currentPosition.longitude,
      );

      CameraPosition cameraPosition = CameraPosition(
        target: latLngPosition,
        zoom: 14,
      );

      await _googleMapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    }
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      /*myLocationEnabled: true,
      zoomControlsEnabled: true,
      zoomGesturesEnabled: true,*/
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
          bottom: kBottomNavigationBarHeight),
      initialCameraPosition: _kGooglePlex,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
        _googleMapController = controller;

        _locatePosition();
      },
    );
  }
}
