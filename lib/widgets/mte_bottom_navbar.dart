import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/blocs/bottom_navigation/app/bottom_navigation_bloc.dart';
import 'package:mytravelingexperience/blocs/bottom_navigation/app/bottom_navigation_event.dart';
import 'package:mytravelingexperience/blocs/bottom_navigation/app/bottom_navigation_state.dart';

class MTEBottomNavigationBar extends StatefulWidget {
  @override
  _MTEBottomNavigationBar createState() => _MTEBottomNavigationBar();
}

class _MTEBottomNavigationBar extends State<MTEBottomNavigationBar> {
  BottomNavigationBloc _bottomNavBloc;

  @override
  void initState() {
    super.initState();
    _bottomNavBloc = BlocProvider.of<BottomNavigationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
      cubit: _bottomNavBloc,
      builder: (BuildContext context, BottomNavigationState state) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black54,
                blurRadius: 1,
                spreadRadius: 0.2,
              ),
            ],
          ),
          child: BottomNavigationBar(
            currentIndex: _bottomNavBloc.currentIndex,
            backgroundColor: Colors.transparent,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.search),
                label: 'Explore',
              ),
              BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.heart),
                label: 'Saved',
              ),
              BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.paperplane),
                label: 'Trips',
              ),
              BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.person),
                label: 'Log in',
              ),
            ],
            onTap: (index) {
              _bottomNavBloc.add(BottomNavigationTapped(index: index));
            },
          ),
        );
      },
    );
  }
}
