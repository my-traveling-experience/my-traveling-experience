import 'package:flutter/material.dart';

class MTEBounce extends StatefulWidget {
  final VoidCallback onPressed;
  final Widget child;
  final Duration duration;

  MTEBounce({
    @required this.child,
    @required this.onPressed,
    this.duration = const Duration(milliseconds: 250),
  }) : assert(child != null);

  @override
  _MTEBounceState createState() => _MTEBounceState();
}

class _MTEBounceState extends State<MTEBounce>
    with SingleTickerProviderStateMixin {
  double _scale;

  AnimationController _animate;

  VoidCallback get _onPressed => widget.onPressed;

  Duration get _duration => widget.duration == null
      ? const Duration(milliseconds: 250)
      : widget.duration;

  @override
  void initState() {
    _animate = AnimationController(
      vsync: this,
      duration: _duration,
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  void dispose() {
    _animate?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _animate.value;
    return GestureDetector(
      onTap: _onPressed != null ? _onTap : null,
      child: Transform.scale(
        scale: _scale,
        child: widget.child,
      ),
    );
  }

  void _onTap() {
    _animate.forward();

    Future.delayed(_duration, () {
      _animate.reverse();

      if (_onPressed != null) _onPressed();
    });
  }
}
