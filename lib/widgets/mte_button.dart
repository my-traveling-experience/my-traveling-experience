import 'package:flutter/material.dart';
import 'package:mytravelingexperience/widgets/mte_jumping_dots.dart';

class MTEElevatedButton extends StatelessWidget {
  final void Function() onPressed;
  final Widget label;
  final bool loading;
  final bool disabled;
  final Widget prefixIcon;
  final Widget suffixIcon;

  const MTEElevatedButton({
    @required this.onPressed,
    @required this.label,
    this.loading = false,
    this.disabled = false,
    this.prefixIcon,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: loading || disabled ? null : onPressed,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xffffbe76),
              Color(0xfff0932b),
            ],
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        padding: EdgeInsets.only(top: 8, bottom: 8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 24.0,
                height: 24.0,
                margin: prefixIcon != null && !loading
                    ? EdgeInsets.only(left: 16.0, right: 16.0)
                    : null,
                child:
                    prefixIcon != null && !loading ? prefixIcon : Container()),
            loading
                ? Container(
                    height: 24.0,
                    padding: EdgeInsets.only(bottom: 7),
                    child: MTEJumpingDotsProgressIndicator(
                      fontSize: 14,
                      color: Theme.of(context).textTheme.button.color,
                    ),
                  )
                : label,
            Container(
              width: 24.0,
              height: 24.0,
              margin: suffixIcon != null && !loading
                  ? EdgeInsets.only(right: 16.0, left: 16.0)
                  : null,
              child: suffixIcon != null && !loading ? suffixIcon : Container(),
            ),
          ],
        ),
      ),
    );
  }
}

class MTEOutlinedButton extends StatelessWidget {
  final void Function() onPressed;
  final Widget label;
  final bool loading;
  final bool disabled;
  final Widget prefixIcon;
  final Widget suffixIcon;

  const MTEOutlinedButton({
    @required this.onPressed,
    @required this.label,
    this.loading = false,
    this.disabled = false,
    this.prefixIcon,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: loading || disabled ? null : onPressed,
      child: Container(
        padding: EdgeInsets.only(top: 8, bottom: 8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 24.0,
                height: 24.0,
                margin: prefixIcon != null && !loading
                    ? EdgeInsets.only(left: 16.0, right: 16.0)
                    : null,
                child:
                    prefixIcon != null && !loading ? prefixIcon : Container()),
            loading
                ? Container(
                    height: 24.0,
                    padding: EdgeInsets.only(bottom: 7),
                    child: MTEJumpingDotsProgressIndicator(
                      fontSize: 14,
                      color: disabled || loading
                          ? null
                          : Theme.of(context).primaryColor,
                    ),
                  )
                : label,
            Container(
              width: 24.0,
              height: 24.0,
              margin: suffixIcon != null && !loading
                  ? EdgeInsets.only(right: 16.0, left: 16.0)
                  : null,
              child: suffixIcon != null && !loading ? suffixIcon : Container(),
            )
          ],
        ),
      ),
    );
  }
}
