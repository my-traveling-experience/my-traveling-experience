import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytravelingexperience/widgets/mte_container.dart';

class MTEContainerScrollable extends StatelessWidget {
  final List<Widget> children;
  final EdgeInsetsGeometry padding;
  final bool scrollbarEnabled;

  MTEContainerScrollable({
    this.padding = const EdgeInsets.all(0),
    this.scrollbarEnabled = false,
    this.children,
  });

  Widget _buildChild(BuildContext context) {
    return _padding(
      context,
      child: _listView(
        context,
        children: children,
      ),
    );
  }

  Widget _padding(BuildContext context, {@required Widget child}) {
    return Padding(
      padding: padding,
      child: child,
    );
  }

  Widget _listView(BuildContext context, {@required List<Widget> children}) {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: children,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MTEContainer(
      child: scrollbarEnabled
          ? CupertinoScrollbar(
              child: _buildChild(context),
            )
          : Container(
              child: _buildChild(context),
            ),
    );
  }
}
