const String API_DOMAIN = "test.api.amadeus.com";
const String API_BASE_URL = "https://" + API_DOMAIN;
const String API_RAW_URL = API_BASE_URL + "/v1";
