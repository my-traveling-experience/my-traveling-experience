import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytravelingexperience/blocs/app/app_event.dart';
import 'package:mytravelingexperience/repositories/firebase_repository.dart';

import 'app.dart';
import 'blocs/app/app_bloc.dart';
import 'blocs/bottom_navigation/app/bottom_navigation_bloc.dart';
import 'blocs/bottom_navigation/app/bottom_navigation_event.dart';

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

Future<Null> _reportError(dynamic error, dynamic stackTrace) async {
  print('Caught error: $error');
  if (isInDebugMode) {
    print(stackTrace);
  }
}

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  final firebaseRepository = FirebaseRepository();

  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isInDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  runZonedGuarded<Future<Null>>(() async {
    runApp(MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AppBloc(firebaseRepository: firebaseRepository),
        ),
        BlocProvider(
          create: (context) =>
              BottomNavigationBloc()..add(BottomNavigationInitialized()),
        ),
      ],
      child: const App(),
    ));
  }, (Object error, StackTrace stackTrace) async {
    await _reportError(error, stackTrace);
  });
}
