import 'package:meta/meta.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:mytravelingexperience/models/user.dart' as model;

class FirebaseProvider {
  final firebase_auth.FirebaseAuth auth = firebase_auth.FirebaseAuth.instance;

  Future<model.User> signUp({
    @required String email,
    @required String username,
    @required String password,
  }) async {
    assert(email != null && password != null);
    try {
      firebase_auth.User user = await auth
          .createUserWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((res) async {
        await res.user.updateProfile(displayName: username);
        await res.user.reload();

        return auth.currentUser;
      });

      if (user != null) {
        return user.toUser;
      }
    } catch (error) {
      rethrow;
    }
    return model.User.empty;
  }

  Future<model.User> logIn({
    @required String email,
    @required String password,
  }) async {
    assert(email != null && password != null);
    try {
      final firebase_auth.UserCredential res =
          await auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      if (res != null) {
        return res.user.toUser;
      }
    } catch (error) {
      rethrow;
    }
    return model.User.empty;
  }

  Future<void> logOut() async {
    try {
      await Future.wait([
        auth.signOut(),
      ]);
    } catch (error) {
      rethrow;
    }
  }

  Stream<model.User> get user {
    return auth.authStateChanges().map((firebaseUser) {
      return firebaseUser == null ? model.User.empty : firebaseUser.toUser;
    });
  }
}

extension on firebase_auth.User {
  model.User get toUser {
    return model.User(
        id: uid, email: email, username: displayName, photo: photoURL);
  }
}
