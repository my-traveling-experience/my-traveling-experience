import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import '../constants.dart';

class ApiException implements Exception {
  final int code;
  final String response;

  const ApiException(this.code, this.response);

  String toString() => this.response;
}

class HttpProvider {
  final String apiBase;

  HttpProvider([this.apiBase = ""]);

  static String base = API_BASE_URL;
  static String token;

  String get baseUrl => base + apiBase;

  Map<String, String> get headers {
    final headers = new Map<String, String>();
    if (token != null && token.isNotEmpty) {
      headers[HttpHeaders.authorizationHeader] = "Bearer " + token;
    }
    return headers;
  }

  dynamic _parseResponse(http.Response res) {
    try {
      dynamic parsedData = jsonDecode(res.body);

      if (res.statusCode >= 400) {
        throw parsedData;
      }
      return parsedData;
    } on FormatException catch (_) {
      throw "Unexpected errors";
    } catch (err) {
      if (err["response"] != null) {
        throw ApiException(err["code"], err["response"]);
      }
      throw err;
    }
  }

  Future<dynamic> fetch(String endpoint) async {
    final res = await http.get(
      baseUrl + Uri.encodeFull(endpoint),
      headers: headers,
    );
    return _parseResponse(res);
  }

  Future<dynamic> post(String endpoint, Map<String, dynamic> payload) async {
    final res = await http.post(
      baseUrl + endpoint,
      body: json.encode(payload),
      headers: headers,
    );
    return _parseResponse(res);
  }

  Future<dynamic> patch(String endpoint, Map<String, dynamic> payload) async {
    final res = await http.patch(
      baseUrl + endpoint,
      body: json.encode(payload),
      headers: headers,
    );
    return _parseResponse(res);
  }

  Future<dynamic> put(String endpoint, Map<String, dynamic> payload) async {
    final res = await http.put(
      baseUrl + endpoint,
      body: json.encode(payload),
      headers: headers,
    );
    return _parseResponse(res);
  }

  Future<dynamic> get(String endpoint) async {
    final res = await http.get(
      baseUrl + endpoint,
      headers: headers,
    );
    return _parseResponse(res);
  }

  Future<dynamic> delete(String endpoint) async {
    final res = await http.delete(
      baseUrl + endpoint,
      headers: headers,
    );
    return _parseResponse(res);
  }
}
