import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MTETheme {
  final Color primaryColor = Color(0xfff0932b);
  final Color primaryColorLight = Color(0xffffbe76);
  final Color accentColor = Color(0xffeb4d4b);
  final Color headlineColor = Color(0xff130f40);
  final Color subtitleColor = Color(0xff130f40);
  final Color bodyTextColor = Color(0xff212529);
  final Color buttonTextColor = Colors.white;

  ThemeData get light {
    return ThemeData.light().copyWith(
      primaryColor: Color(0xfff0932b),
      primaryColorLight: Color(0xffffbe76),
      accentColor: Color(0xffeb4d4b),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
              (Set<MaterialState> states) {
            return RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            );
          }),
          side: MaterialStateProperty.resolveWith<BorderSide>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled)) {
                return BorderSide(
                  color: Colors.grey,
                  width: 2,
                );
              }
              return BorderSide(
                color: primaryColor,
                width: 2,
              );
            },
          ),
          overlayColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.focused))
              return Colors.transparent;
            if (states.contains(MaterialState.hovered))
              return Colors.transparent;
            if (states.contains(MaterialState.pressed))
              return Colors.transparent;
            return null;
          }),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled))
              return Colors.transparent;
            return null; // Defer to the widget's default.
          }),
          foregroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) return Colors.grey;
            return primaryColor; // Defer to the widget's default.
          }),
          padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
              (Set<MaterialState> states) {
            return EdgeInsets.all(0.0);
          }),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
              (Set<MaterialState> states) {
            return RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            );
          }),
          overlayColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.focused))
              return Colors.transparent;
            if (states.contains(MaterialState.hovered))
              return Colors.transparent;
            if (states.contains(MaterialState.pressed))
              return primaryColorLight;
            return null;
          }),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) return Colors.grey;
            return null;
          }),
          foregroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) return Colors.grey;
            return Colors.white;
          }),
          padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
              (Set<MaterialState> states) {
            return EdgeInsets.all(0.0);
          }),
        ),
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        unselectedItemColor: Colors.grey,
        selectedLabelStyle: TextStyle(
          fontWeight: FontWeight.bold,
        ),
        selectedIconTheme: IconThemeData(
          color: primaryColor,
        ),
        elevation: 0,
      ),
      textTheme: TextTheme(
        headline1: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 48,
            fontWeight: FontWeight.bold,
          ),
        ),
        headline2: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 42,
            fontWeight: FontWeight.bold,
          ),
        ),
        headline3: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 34,
            fontWeight: FontWeight.bold,
          ),
        ),
        headline4: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        headline5: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        headline6: GoogleFonts.robotoSlab(
          textStyle: TextStyle(
            color: headlineColor,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle1: GoogleFonts.ubuntu(
          textStyle: TextStyle(
            color: subtitleColor,
            fontSize: 16,
            fontWeight: FontWeight.normal,
          ),
        ),
        subtitle2: GoogleFonts.ubuntu(
          textStyle: TextStyle(
            color: subtitleColor,
            fontSize: 14,
            fontWeight: FontWeight.normal,
          ),
        ),
        bodyText1: GoogleFonts.roboto(
          textStyle: TextStyle(
            color: bodyTextColor,
            fontSize: 20,
            fontWeight: FontWeight.normal,
          ),
        ),
        bodyText2: GoogleFonts.roboto(
          textStyle: TextStyle(
            color: bodyTextColor,
            fontSize: 16,
            fontWeight: FontWeight.normal,
          ),
        ),
        button: GoogleFonts.roboto(
          textStyle: TextStyle(
            color: buttonTextColor,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
