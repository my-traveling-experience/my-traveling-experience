import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mytravelingexperience/screens/home_screen.dart';
import 'package:mytravelingexperience/screens/splash_screen.dart';

import 'themes/theme.dart';
import 'blocs/app/app_bloc.dart';
import 'blocs/app/app_state.dart';
import 'generated/l10n.dart';

class App extends StatefulWidget {
  const App({Key key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

  Widget _buildScreen(BuildContext context, AppState state) {
    if (state is AppUninitialized) {
      return SplashScreen();
    } else if (state is AppUnauthenticated) {
      return HomeScreen(user: null);
    } else if (state is AppLoading) {
      return HomeScreen(loading: true);
    } else if (state is AppAuthenticated) {
      return HomeScreen(user: state.user);
    } else {
      return SplashScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'My Traveling Experience',
      debugShowCheckedModeBanner: false,
      theme: MTETheme().light,
      darkTheme: MTETheme().light,
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      home: BlocBuilder<AppBloc, AppState>(
          builder: (BuildContext context, AppState state) {
        return AnimatedSwitcher(
            duration: Duration(milliseconds: 5000),
            child: _buildScreen(context, state),
            transitionBuilder: (Widget child, Animation<double> animation) {
              Widget fadeChild;
              if (animation.status == AnimationStatus.dismissed) {
                fadeChild = ScaleTransition(
                  scale: Tween<double>(begin: 0.95, end: 1).animate(animation),
                  child: child,
                );
              } else {
                fadeChild = child;
              }
              return FadeTransition(
                opacity: animation,
                child: fadeChild,
              );
            });
      }),
    );
  }
}
