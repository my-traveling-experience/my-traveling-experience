// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "homePage" : MessageLookupByLibrary.simpleMessage("Home"),
    "loginBackToLogin" : MessageLookupByLibrary.simpleMessage("Back to login"),
    "loginConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirm password"),
    "loginConfirmPasswordEmpty" : MessageLookupByLibrary.simpleMessage("You must confirm your password"),
    "loginConfirmPasswordIdentical" : MessageLookupByLibrary.simpleMessage("Paswords must be identical"),
    "loginEmailAddress" : MessageLookupByLibrary.simpleMessage("Email address"),
    "loginEmailAddressEmpty" : MessageLookupByLibrary.simpleMessage("You must enter an email address"),
    "loginFoundIt" : MessageLookupByLibrary.simpleMessage("Oh, I found it. It\'s okay"),
    "loginHaveAccount" : MessageLookupByLibrary.simpleMessage("Already have an account"),
    "loginNoAccount" : MessageLookupByLibrary.simpleMessage("Don\'t have an account yet?"),
    "loginPassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "loginPasswordEmpty" : MessageLookupByLibrary.simpleMessage("You must enter a password"),
    "loginRegisterEmail" : MessageLookupByLibrary.simpleMessage("Register with Email"),
    "loginSignInEmail" : MessageLookupByLibrary.simpleMessage("Sign in with Email"),
    "loginUsername" : MessageLookupByLibrary.simpleMessage("Username"),
    "loginUsernameEmpty" : MessageLookupByLibrary.simpleMessage("You must enter an username")
  };
}
