// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "homePage" : MessageLookupByLibrary.simpleMessage("Home"),
    "loginBackToLogin" : MessageLookupByLibrary.simpleMessage("Retour à la connexion"),
    "loginConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirmez votre mot de passe"),
    "loginConfirmPasswordEmpty" : MessageLookupByLibrary.simpleMessage("Vous devez confirmez votre mot de passe"),
    "loginConfirmPasswordIdentical" : MessageLookupByLibrary.simpleMessage("Les mots de passe doivent être identiques"),
    "loginEmailAddress" : MessageLookupByLibrary.simpleMessage("Addresse email"),
    "loginEmailAddressEmpty" : MessageLookupByLibrary.simpleMessage("Vous devez entrer une addresse email"),
    "loginFoundIt" : MessageLookupByLibrary.simpleMessage("Oh, je l\'ai trouvé. C\'est bon"),
    "loginHaveAccount" : MessageLookupByLibrary.simpleMessage("J\'ai déjà un compte"),
    "loginNoAccount" : MessageLookupByLibrary.simpleMessage("Je n\'ai pas encore de compte ?"),
    "loginPassword" : MessageLookupByLibrary.simpleMessage("Mot de passe"),
    "loginPasswordEmpty" : MessageLookupByLibrary.simpleMessage("Vous devez entrer un mot de passe"),
    "loginRegisterEmail" : MessageLookupByLibrary.simpleMessage("S\'inscrire par Email"),
    "loginSignInEmail" : MessageLookupByLibrary.simpleMessage("Se connecter par Email"),
    "loginUsername" : MessageLookupByLibrary.simpleMessage("Nom d\'utilisateur"),
    "loginUsernameEmpty" : MessageLookupByLibrary.simpleMessage("Vous devez entrer un nom d\'utilisateur")
  };
}
