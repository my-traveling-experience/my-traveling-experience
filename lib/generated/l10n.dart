// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Home`
  String get homePage {
    return Intl.message(
      'Home',
      name: 'homePage',
      desc: '',
      args: [],
    );
  }

  /// `Email address`
  String get loginEmailAddress {
    return Intl.message(
      'Email address',
      name: 'loginEmailAddress',
      desc: '',
      args: [],
    );
  }

  /// `You must enter an email address`
  String get loginEmailAddressEmpty {
    return Intl.message(
      'You must enter an email address',
      name: 'loginEmailAddressEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get loginUsername {
    return Intl.message(
      'Username',
      name: 'loginUsername',
      desc: '',
      args: [],
    );
  }

  /// `You must enter an username`
  String get loginUsernameEmpty {
    return Intl.message(
      'You must enter an username',
      name: 'loginUsernameEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get loginPassword {
    return Intl.message(
      'Password',
      name: 'loginPassword',
      desc: '',
      args: [],
    );
  }

  /// `You must enter a password`
  String get loginPasswordEmpty {
    return Intl.message(
      'You must enter a password',
      name: 'loginPasswordEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password`
  String get loginConfirmPassword {
    return Intl.message(
      'Confirm password',
      name: 'loginConfirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `You must confirm your password`
  String get loginConfirmPasswordEmpty {
    return Intl.message(
      'You must confirm your password',
      name: 'loginConfirmPasswordEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Paswords must be identical`
  String get loginConfirmPasswordIdentical {
    return Intl.message(
      'Paswords must be identical',
      name: 'loginConfirmPasswordIdentical',
      desc: '',
      args: [],
    );
  }

  /// `Already have an account`
  String get loginHaveAccount {
    return Intl.message(
      'Already have an account',
      name: 'loginHaveAccount',
      desc: '',
      args: [],
    );
  }

  /// `Oh, I found it. It's okay`
  String get loginFoundIt {
    return Intl.message(
      'Oh, I found it. It\'s okay',
      name: 'loginFoundIt',
      desc: '',
      args: [],
    );
  }

  /// `Don't have an account yet?`
  String get loginNoAccount {
    return Intl.message(
      'Don\'t have an account yet?',
      name: 'loginNoAccount',
      desc: '',
      args: [],
    );
  }

  /// `Back to login`
  String get loginBackToLogin {
    return Intl.message(
      'Back to login',
      name: 'loginBackToLogin',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with Email`
  String get loginSignInEmail {
    return Intl.message(
      'Sign in with Email',
      name: 'loginSignInEmail',
      desc: '',
      args: [],
    );
  }

  /// `Register with Email`
  String get loginRegisterEmail {
    return Intl.message(
      'Register with Email',
      name: 'loginRegisterEmail',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'fr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}