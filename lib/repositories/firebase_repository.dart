import 'package:meta/meta.dart';
import 'package:mytravelingexperience/models/user.dart' as model;
import 'package:mytravelingexperience/providers/firebase_provider.dart';

class FirebaseRepository {
  final _firebaseProvider = FirebaseProvider();

  Future<model.User> logIn({
    @required String email,
    @required String password,
  }) {
    try {
      return _firebaseProvider.logIn(email: email, password: password);
    } catch (error) {
      rethrow;
    }
  }

  Future<model.User> signUp({
    @required String email,
    @required String username,
    @required String password,
  }) {
    try {
      return _firebaseProvider.signUp(
          email: email, username: username, password: password);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> logOut() {
    try {
      return _firebaseProvider.logOut();
    } catch (error) {
      rethrow;
    }
  }

  Stream<model.User> get user => _firebaseProvider.user;
}
