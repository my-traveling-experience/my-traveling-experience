import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User implements Equatable {
  final String id;
  final String email;
  final String username;
  final String photo;

  const User({
    @required this.id,
    @required this.email,
    @required this.username,
    @required this.photo,
  })  : assert(email != null),
        assert(id != null);

  static const empty = User(email: '', id: '', username: null, photo: null);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [email, id, username, photo];
}
